/*
 * main.c
 * Copyright (C) Valery Tolstov 2012 <mtcomscxstart@gmail.com>
 * 
smallcalc is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * smallcalc is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

char buf[80];
int  p;
bool ok;

int expr();
int term();
int factor();

int expr() {
	int r = 0;
	if (buf[p] != '+' && buf[p] != '-')
		r = term();
	while (buf[p] == '+' || buf[p] == '-')
		r += (buf[p++] == '+' ? term() : -term());
	return r;
}

int term() {
	int r = factor();
	while (buf[p] == '*' || buf[p] == '/')
		r = (buf[p++] == '*' ? r * factor() : r / factor());
	return r;
}

int factor() {
	int r = 0;
	if (buf[p] == '(') {
		p++;
		r = expr();
		if (buf[p] == ')')
			p++;
	} else if (buf[p] >= '0' && buf[p] <= '9')
		while (buf[p] >= '0' && buf[p] <= '9')
			r = r * 10 + buf[p++] - '0';
	else
		ok = false;
	return r;
}

void low(char *s) {
	while (*s) {
		*s = tolower(*s);
		s++;
	}
}

int main() {
	puts("Hello! This is a simple expression calculator");
	for (; ; ) {
		printf("> ");
		gets(buf);
		low(buf);
		if (strcmp(buf, "quit") == 0 ||
			strcmp(buf, "exit") == 0)
			return 0;
		p = 0, ok = true;
		int r = expr();
		if (buf[p] != 0 || !ok)
			puts("Incorrect expression");
		else
			printf("%d\n", r);
	}
	return 0;
}
